<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/10/2019
 * Time: 22:46
 */

namespace Flyshot\ApiUtilsBundle\Response;

class Error implements \JsonSerializable
{
    const UNEXPECTED_ERROR = 'unexpected_error';
    const RESOURCE_MISSING = 'resource_missing';
    const PARAM_MISSING = 'param_missing';
    const PARAM_INVALID = 'param_invalid';

    protected $message;
    protected $type;

    public function __construct(string $type, string $message = null)
    {
        $this->message = $message;
        $this->type = $type;
    }

    public function jsonSerialize()
    {
        return [
            'message' => $this->message,
            'type' => $this->type,
        ];
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMessage()
    {
        return $this->message;
    }
}