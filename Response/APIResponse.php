<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 20/01/2019
 * Time: 17:33
 */

namespace Flyshot\ApiUtilsBundle\Response;

use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;

class APIResponse extends JsonResponse
{
    public function __construct(SerializerInterface $serializer, $data = null, $error = null, $groups = [], $version = '')
    {
        $oldError = $error;
        if ($error instanceof Error) {
            $oldError = $error->getType();
        }

        $data = [
            'data' => $data,
            'errors' => (array)$oldError,
            'error' => $error,
            'meta' => [
                'generated' => time(),
            ]
        ];

        $context = SerializationContext::create()->setVersion($version);
        $context->setSerializeNull(true);
        if ($groups) {
            $context->setGroups($groups);
        }
        $jsonData = $serializer->serialize(
            $data,
            'json',
            $context);

        $status = (empty($error)) ? self::HTTP_OK : self::HTTP_BAD_REQUEST;

        parent::__construct($jsonData, $status, [], true);
    }
}